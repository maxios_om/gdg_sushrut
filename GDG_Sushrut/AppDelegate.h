//
//  AppDelegate.h
//  GDG_Sushrut
//
//  Created by Om's on 11/07/15.
//  Copyright (c) 2015 Oms. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

