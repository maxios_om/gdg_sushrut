//
//  main.m
//  GDG_Sushrut
//
//  Created by Om's on 11/07/15.
//  Copyright (c) 2015 Oms. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
